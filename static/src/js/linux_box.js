import $ from 'jquery';
import {url} from './const.js';

function postServerCmd() {
    let cmd = JSON.stringify({"cmd": $('#text-linux').val()});
    $.ajax({
        "type": "POST",
        "url": url + "/home/sendcmd",

        "contentType": "application/json",
        "data": cmd,
        "success": suc,

    });
}

function logoutUser() {
    $.ajax({
        "type": "POST",
        "url": url + "/home/logout",

        "contentType": "application/json",
        "success": transferUser,

    });
}

function suc() {

}

function transferUser(response) {
    if (response == "success")
        window.location.replace(url + "/");
}

export {postServerCmd, logoutUser};