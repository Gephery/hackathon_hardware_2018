#### WSU Hardware hackathon Ghost Club server code. 
### Intent 
To make a multiple smart home web based communication channel. This 
project was done in a 24hr time span so the documentation is not great.  
### Flow 
On a very high level the program has two systems, one the **web server**, 
and two the **home device server** server. Although both are TCP 
connections, the main purpose of splitting the tcp server is to allow 
less security and more flexibility on the device side. The web server
communicates with the home device server to change home devices such as 
lights. 
### Flow of Web server
Simple and unsecured web server that allows sign in and the backend
for adding a user to RFID tag association. 
### What was learned 
Golang has a unique approach to asynchronous tasks. The Echo 
framework is very lightweight, and does little.  
### Author 
Max Grucella (Gephery)
