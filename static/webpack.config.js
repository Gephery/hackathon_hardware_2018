// This library allows us to combine paths easily
const path = require('path');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin'); // Require  html-webpack-plugin plugin


module.exports = {
   entry: {
       "home": [path.resolve(__dirname, 'src/js', 'index.js'),
             path.resolve(__dirname, 'src/css', 'index.scss')],
       "signIn": [path.resolve(__dirname, 'src/js', 'sign.js'),
             path.resolve(__dirname, 'src/css', 'index.scss')]
   },
   output: {
      path: path.resolve(__dirname, 'output'),
      filename: 'output/[name].min.js'
   },
   plugins: [
      new MiniCssExtractPlugin({
		    // Options similar to the same options in webpackOptions.output
		    // both options are optional
		    filename: "output/[name].min.css",
		  }),
      new HtmlWebpackPlugin({
          template: __dirname + "/src/html/index.html",
          inject: 'body',
          chunks: ['home'],
          filename: "index.html"
      }),
       new HtmlWebpackPlugin({
           template: __dirname + "/src/html/sign.html",
           inject: 'body',
           chunks: ['signIn'],
           filename: "sign.html"
       })
   ],
   module: {
     rules: [
       {
         test: /\.js?$/,
         use: {
           loader: 'babel-loader',
           query: {
             presets: ['es2015']
           }
         }
        },
        {
          test: /\.(scss|css)$/,
            use: [
                {loader: MiniCssExtractPlugin.loader},
                {loader: "css-loader"}, // translates CSS into CommonJS
                {loader: "postcss-loader",
                 options: {
                   plugins: function() {
                     return [
                       require("precss"),
                       require("autoprefixer")
                     ]
                   }
                 }
                },
                {loader: "sass-loader"}, // compiles Sass to CSS, using Node Sass by default
            ]
        }
     ]
    },
};

