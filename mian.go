package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"fmt"
	"net"
	"bufio"
	"encoding/binary"
	"time"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"strconv"
)

const SECRET = "sankyu"

const TCP_PORT = ":7650"
const HTML_PORT = ":8760"

const END_CHAR byte = 33
const START_CHAR byte = 36

const DATA_NUM byte = 'n'
const DATA_STR byte = 's'

type LinBoxEvent struct {
  ID, Type, DataType byte
  DataString string
  DataBin uint32
  RoomID uint32

}

type UserInfo struct {
	RoomID uint32
	IsLoggedOn bool
}

type BackCreate struct {
	Success bool
}


var db, dbErr = sql.Open("mysql", "root:1122HI@tcp(127.0.0.1:3306)/hack2018")
var mostRecent uint32 = 1

//func test(actionQueue chan LinBoxEvent) {
//	for {
//		actionQueue <- LinBoxEvent{}
//		time.Sleep(500 * time.Millisecond)
//	}
//}

func main() {
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	defer db.Close()
	eventQueue := make(chan LinBoxEvent, 50)
	actionQueue := make(chan LinBoxEvent, 50)
	//var connectedClients list.List
	go TCPSetup(eventQueue, actionQueue)
	//actionQueue <- LinBoxEvent{Type: 'a', ID: 39, DataType: 'n', DataBin: 8}


	// Web server
	e := echo.New()
	g := e.Group("/home")
	e.Use(session.Middleware(sessions.NewCookieStore([]byte("secret"))))
	//g.Use(middleware.BasicAuth(func(username, passwordIN string, c echo.Context) (bool, error) {
	//	var password string
	//	row := db.QueryRow("select password from users where username = ?;", username)
	//	err := row.Scan(&password)
	//	if err != nil {
	//		return false, nil
	//	}
	//	fmt.Println(password)
	//	if password == passwordIN {
	//		sess, _ := session.Get("session", c)
	//		sess.Options = &sessions.Options{
	//			Path:     "/",
	//			MaxAge:   86400 * 7,
	//			HttpOnly: true,
	//		}
	//		sess.Values["user"] = username
	//		sess.Save(c.Request(), c.Response())
	//		return true, nil
	//	}
	//	return false, nil
	//
	//}))
	e.Use(middleware.AddTrailingSlash())
	e.POST("/login", func(c echo.Context) error {
		uInfo := echo.Map{}
		if err := c.Bind(&uInfo); err != nil {
			return err
		}
		fmt.Println(uInfo)
		username := uInfo["username"]
		passwordIN := uInfo["password"]
		var password string
		row := db.QueryRow("select password from users where username = ?;", username)
		err := row.Scan(&password)
		if err != nil {
			return c.String(http.StatusOK, "failed")
		}
		fmt.Println(password)
		if password == passwordIN {
			sess, _ := session.Get("session", c)
			sess.Options = &sessions.Options{
				Path:     "/",
				MaxAge:   8000 * 7,
				HttpOnly: true,
			}
			sess.Values["user"] = username
			sess.Save(c.Request(), c.Response())
			db.Exec("update users set loggedIn = 1 where username = ?;", sess.Values["user"])
			return c.String(http.StatusOK, "success")
		}
		return c.String(http.StatusOK, "failed")
	})
	g.POST("/logout", func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		sess.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   8000 * 7,
			HttpOnly: true,
		}


		// Check if user exists then changed logged in
		fmt.Println(sess.Values["user"])
		row := db.QueryRow("select username from users where username = ?;", sess.Values["user"])
		var username string
		err := row.Scan(&username)
		if err != nil {
			return c.String(http.StatusOK, "failed")
		}
		db.Exec("update users set loggedIn = 0 where username = ?;", sess.Values["user"])
		sess.Values["user"] = nil
		sess.Save(c.Request(), c.Response())

		return c.String(http.StatusOK, "success")
	})
	e.GET("/", func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		fmt.Println(sess.Values["user"])
		if sess.Values["user"] != nil {
			filename := "static/output/sign.html"
			return c.File(filename)
		} else {
			filename := "static/output/index.html"
			return c.File(filename)
		}
	})
	e.Static("/output", "static/output/output")
	g.GET("/", func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		fmt.Println(sess.Values["user"])
		if sess.Values["user"] != nil {
			filename := "static/output/sign.html"
			return c.File(filename)
		} else {
			filename := "static/output/index.html"
			return c.File(filename)
		}
	})
	g.Static("/output", "static/output/output")
	e.POST("/account/create", func(c echo.Context) error {
		uInfo := echo.Map{}
		if err := c.Bind(&uInfo); err == nil {
			return err
		}
		fmt.Println(uInfo)
		username := uInfo["username"]
		password := uInfo["password"]
		rfid := uInfo["rfid"]
		iconid := uInfo["iconid"]
		sess, _ := session.Get("session", c)
		sess.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   8000 * 7,
			HttpOnly: true,
		}
		sess.Values["user"] = username
		sess.Save(c.Request(), c.Response())
		//b := &BackCreate {
		//	Success: true,
		//}
		db.Exec("insert into users (username, password, rfid, iconid, loggedIn) values (?, ?, ?, ?, 1);", username, password, rfid, iconid)
		return c.String(http.StatusOK, "success")
	})
	g.POST("/sendcmd/", func(c echo.Context) error {
		cmd := echo.Map{}
		if err := c.Bind(&cmd); err != nil {
			return err
		}

		//msg := cmd["cmd"].(string)
		fmt.Println("Writing...")
		actionQueue <- LinBoxEvent{Type: 'a', ID: 2, DataType: 'n', DataBin: 77, RoomID: 5}

		return c.String(http.StatusOK, "hello admin!")
	})
	g.GET("/user/info/recent", func (c echo.Context) error {
		fmt.Println(strconv.Itoa(int(mostRecent)))
		return c.String(http.StatusOK, string(strconv.Itoa(int(mostRecent))))
	})
	g.POST("/user/info", func (c echo.Context) error {
		sess, _ := session.Get("session", c)
		sess.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   8000 * 7,
			HttpOnly: true,
		}
		user := sess.Values["user"]
		var roomId uint32
		var loggedIn bool
		db.QueryRow("select rfids.loggedIn, rfids.roomId from rfids inner join users on users.rfid=rfids.rfid where users.username=?", user).Scan(&loggedIn, &roomId)
		uI := &UserInfo {
			RoomID: roomId,
			IsLoggedOn: loggedIn,
		}
		return c.JSON(http.StatusOK, uI)
	})
	defer fmt.Println("wherer")
	e.Logger.Fatal(e.Start(HTML_PORT))
}

func handleQueuedItems(eventQueue chan LinBoxEvent, actionQueue chan LinBoxEvent) {
	for {
		event := <- eventQueue
		fmt.Println("handling queue")
		var rfid = -1
		switch event.ID {
		case 0:
			fmt.Println("logging in")
			row := db.QueryRow("select rfid from rfids where rfid = ?;", event.DataBin)
			row.Scan(&rfid)
			if rfid == -1 {
				db.Exec("insert into rfids (rfid, loggedIn, roomId) values (?, 1, ?);", event.DataBin, event.RoomID)
				fmt.Println("create")
			} else {
				fmt.Println("update")
				db.Exec("update rfids set loggedIn = 1, roomId = ? where rfid = ?;", event.RoomID, event.DataBin)
			}
		case 1:
			fmt.Println("logout")
			row := db.QueryRow("select rfid from rfids where rfid = ?;", event.DataBin)
			row.Scan(&rfid)
			if rfid == -1 {
				db.Exec("insert into rfids (rfid, loggedIn, roomId) values (?, 0, ?);", event.DataBin, event.RoomID)
			} else {
				db.Exec("update rfids set loggedIn = 0, roomId = ? where rfid = ?;", event.RoomID, event.DataBin)
			}
		}
		mostRecent = event.DataBin
		actionQueue <- LinBoxEvent{Type: 'i', ID: 0, DataType: 'n', DataBin: 77}
	}
}

func TCPSetup(eventQueue chan LinBoxEvent, actionQueue chan LinBoxEvent) {
	var i uint32 = 0
	lister, _ := net.Listen("tcp4", TCP_PORT)
	fmt.Println("waiting...")
	go handleQueuedItems(eventQueue, actionQueue)

	for {
		fmt.Println(i)
		conn, _ := lister.Accept()
		go handleRequest(conn, eventQueue, actionQueue)
		i = i + 1

	}
}

func handleRequest(conn net.Conn, eventQueue chan LinBoxEvent, actionQueue chan LinBoxEvent) {
	fmt.Println("got new TCP")
	_, result := readTCPPacket(conn)
	_, result2 := readTCPPacket(conn)
	fmt.Println("read new tcp")
	response := decodePacket(result)
	if response.DataString != SECRET {
		fmt.Println("no good secret")
		fmt.Println(result)
	} else {
		numPack := decodePacket(result2)
		id := numPack.DataBin
		fmt.Print("ID: ")
		fmt.Println(id)
		fmt.Println(response.DataString)
		writeTCPPacketN(conn, 'i', 39, 'n', id)
		quit := make(chan bool, 1)

		go handleTCPRequests(conn, eventQueue, quit, id)
		go handleTCPOut(conn, actionQueue, quit, id)
		//TODO decode and check if secret okay
		//TODO write info from channel
		isQuit := false
		for {
			select {
			case isQuit = <-quit:
			default:
			}
			if isQuit {
				break
			}

			//writeTCPPacketN(conn, 'i', 39, 'n', 7)
		}
	}
	conn.Close()
	fmt.Println("ending!!")
}

func handleTCPOut(conn net.Conn, actionQueue chan LinBoxEvent, quit chan bool, id uint32) {
	var isErr bool
	for {
		select {
		case action := <-actionQueue:
			if action.RoomID == id {
				isErr = writeTCPPacket(conn, action)
			} else {
				actionQueue <- action
			}
			break
		default:
		}
		if isErr {
			fmt.Println("quit please")
			break
		}
		time.Sleep(time.Millisecond * 50)

	}
	//quit <- true
}

func handleTCPRequests(conn net.Conn, eventQueue chan LinBoxEvent, quit chan bool, id uint32) {
	for {
		ok, result := readTCPPacket(conn)
		fmt.Println(string(result))
		//TODO decode and add to queue
		if !ok {
			break
		} else {
			fmt.Println("added event to queue")
			packet := decodePacket(result)
			packet.RoomID = id
			eventQueue <- packet
		}
		time.Sleep(time.Millisecond * 50)
	}

	quit <- true
}

func decodePacket(info []byte) LinBoxEvent {
	var data LinBoxEvent
	fmt.Println(info[0])
	data.Type = info[1]
	data.ID = info[2]
	data.DataType = info[3]
	if data.DataType == DATA_NUM {
		data.DataBin = binary.BigEndian.Uint32(info[4:len(info)-1])
	} else {
		data.DataString = string(info[4:len(info)-1])
	}
	fmt.Println(data)
	return data
}

func writeTCPPacket(conn net.Conn, packet LinBoxEvent) bool {
	if packet.DataType == DATA_NUM {
		return writeTCPPacketN(conn, packet.Type, packet.ID, packet.DataType, packet.DataBin)
	} else if packet.DataType == DATA_STR {
		return writeTCPPacketS(conn, packet.Type, packet.ID, packet.DataType, packet.DataString)
	}
	fmt.Println("you sending bad")
	return true
}

func writeTCPPacketS(conn net.Conn, pType, pID, dataType byte, data string) bool {
	writeData := make([]byte, 8)
	writeData[0] = START_CHAR
	writeData[1] = pType
	writeData[2] = pID
	writeData[3] = dataType
	strBytes := []byte(data)
	for i := 0; i < len(strBytes); i++ {
		writeData[4+i] = strBytes[i]
	}
	_, err := conn.Write(writeData)
	fmt.Println(err)
	return err != nil
}

func writeTCPPacketN(conn net.Conn, pType, pID, dataType byte, data uint32) bool {
	bin := make([]byte, 4)
	binary.BigEndian.PutUint32(bin, data)
	writeData := make([]byte, 9)
	writeData[0] = START_CHAR
	writeData[1] = pType
	writeData[2] = pID
	writeData[3] = dataType
	writeData[4] = bin[0]
	writeData[5] = bin[1]
	writeData[6] = bin[2]
	writeData[7] = bin[3]
	writeData[8] = END_CHAR
	_, err := conn.Write(writeData)
	fmt.Println(err)
	return err != nil

}

func readTCPPacket(conn net.Conn) (bool, []byte) {
	connBuff := bufio.NewReader(conn)
	resultT, err := connBuff.ReadBytes(END_CHAR)

	if err != nil {
		fmt.Println(err)
		return false, nil
	}
	return true, resultT
}
