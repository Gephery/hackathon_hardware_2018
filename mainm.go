package main

import (
	"os"
	"fmt"
	"net"
	"bufio"
)

func main() {
	//if len(os.Args) != 2 {
	//	fmt.Fprintf(os.Stderr, "Usage: %s host:port ", os.Args[0])
	//	os.Exit(1)
	//}
	service := ":7650"
	//var result string
	//var result = make([]byte, 4)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)

	// Read Data
	conn.Write([]byte("$e'nsankyu!"))
	connBuff := bufio.NewReader(conn)
	result, _ := connBuff.ReadBytes('!')
	fmt.Println(result)

	checkError(err)
	//bufio.NewReader(conn).Read(result)
	fmt.Println(conn.RemoteAddr())

	//readTCP(conn)
	//writeTCP(conn, "$kslfj!")
	//time.Sleep(10 * time.Millisecond)


	fmt.Println("done.")
	conn.Close()

	os.Exit(0)
}

func writeTCP(conn net.Conn, msg string) {
	conn.Write([]byte(msg))
}

func readTCP(conn net.Conn) {
	connBuff := bufio.NewReader(conn)
	result, _ := connBuff.ReadBytes('!')
	fmt.Println(result)
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}