import {postServerCmd, logoutUser} from './linux_box.js';
import 'bootstrap';
import $ from 'jquery';
import {url} from "./const";

(function() {
    "use strict";
    var timer;
    var cc = 0;

    $(window).on("load", bindLoad);

    function bindLoad() {
        $('#submit-linux').on("click", postServerCmd);

        $('.geph-ui-third').height(screen.height/3);
        //$('.geph-ui-third').width(screen.width/3);

        $('.geph-ui-drop').on("click", timeTextButtonSwitcher);

        $('#logout-user').on("click", logoutUser);

        $('.geph-toggle-cmd').on("click", toggleLight);

        setInterval(showMostRecent, 1500);
    }

    function showMostRecent() {
        $.ajax({
            "type": "GET",
            "url": url + "/home/user/info/recent",
            "success": showshowMostRecent

        });
    }

    function showshowMostRecent(response) {
        $('.showshow').val(parseInt(response));
    }

    function toggleLight() {
        $.ajax({
            "type": "POST",
            "url": url + "/home/sendcmd/",

            "contentType": "application/json",
            "data": JSON.stringify({"cmd": "hi"}),

        });
    }

    function timeTextButtonSwitcher(e) {
      let box = $(e.target).closest('.geph-ui-container').find('.geph-ui-box');
      clearInterval(timer);
      e.stopPropagation();
      cc++;
      if (e.target.classList.contains('geph-id-btn')) {
          let el  = $('<input id="geph-toggle'+ cc + '" class="btn btn-primary col-4" type="button" value="light">');
          box.html(el);
          $('#geph-toggle' + cc).on("click", toggleLight);

      } else if (e.target.classList.contains('geph-id-txt'))
        box.html('<div class="form-group"><label for="exampleFormControlTextarea1">Most Recent Scan</label>' +
                  '<textarea class="showshow form-control" id="exampleFormControlTextarea1" rows="3"></textarea></div>');
      else if (e.target.classList.contains('geph-id-time')) {
        timer = setInterval(updateTime, 100);
        let d = new Date();
        box.html('<span class="geph-timer">' + d.toLocaleTimeString() + '</span>');
      } else
        box.html('error');
    }

    function updateTime() {
      let d = new Date();
      $('.geph-timer').html(d.toLocaleTimeString());
    }
})();
