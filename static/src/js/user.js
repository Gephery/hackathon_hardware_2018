import $ from "jquery";
import {url} from "./const.js"

function createUser(e) {
    let username = $('#text-username').val();
    let password = $('#text-password').val();
    let rfid = $('#text-rfid').val();
    let iconid = $('#text-iconid').val();
    $.ajax({
        "type": "POST",
        "url": url + "/account/create",

        "contentType": "application/json",
        "data": JSON.stringify({"username": username, "password": password, "rfid": rfid, "iconid": iconid}),
        "success": transferUser,

    });
}

function signUserIn(e) {
    let username = $('#sign-username').val();
    let password = $('#sign-password').val();
    $.ajax({
        "type": "POST",
        "url": url + "/login",

        "contentType": "application/json",
        "data": JSON.stringify({"username": username, "password": password}),
        "success": transferUser,

    });
}

function transferUser(response) {
    if (response == "success")
        window.location.replace(url + "/home/");
}

// function suc() {
//     debugger;
// }

export {createUser, signUserIn};