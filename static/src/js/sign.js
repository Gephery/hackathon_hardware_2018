import 'bootstrap';
import $ from 'jquery';
import {createUser, signUserIn} from "./user";

(function() {
    "use strict";

    $(window).on("load", bindLoad);

    function bindLoad() {
        $('#submit-user').on("click", createUser);
        $('#submit-sign').on("click", signUserIn);

    }

})();
